# Steps

- Start docker: `docker-compose up -d`
- Execute schema.sql
- Run project



# Limitation 
- Additional activity log fields must be a column of a table
- Manually add trigger to the table may require
- Adding table require mapping column into activity log response view