------------- Schema
DROP VIEW ACTIVITY_LOG_VIEW;
DROP TABLE ACTIVITY_LOG;
SELECT * FROM ACTIVITY_LOG;
CREATE TABLE ACTIVITY_LOG
(
    ID          UUID PRIMARY KEY,
    TABLE_NAME  VARCHAR(50) NOT NULL,
    RECORD_ID   UUID NOT NULL,
    ACTION      VARCHAR(10) NOT NULL,
    CREATED_AT  TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CREATED_BY  VARCHAR(50) DEFAULT 'SYS',
    UPDATED_AT  TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
    UPDATED_BY  VARCHAR(50) DEFAULT 'SYS'
);

DROP TABLE ACTIVITY_LOG_DETAIL;
SELECT * FROM ACTIVITY_LOG_DETAIL;
CREATE TABLE ACTIVITY_LOG_DETAIL
(
    ID          UUID PRIMARY KEY,
    KEY         VARCHAR(50) NOT NULL ,
    OLD_VALUE       VARCHAR(1000),
    NEW_VALUE       VARCHAR(1000),
    ACTIVITY_LOG_ID UUID NOT NULL
);

DROP TABLE AGENCY;
SELECT * FROM AGENCY;
CREATE TABLE AGENCY
(
    ID          UUID PRIMARY KEY,
    NAME        VARCHAR(50) NOT NULL,
    EMAIL       VARCHAR(100),
    ADDRESS     VARCHAR(500),
    CREDIT      DECIMAL(10, 2)
);




--- UUID and CROSSTAB function
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS tablefunc;



CREATE OR REPLACE FUNCTION activity_log()
RETURNS TRIGGER AS $$


DECLARE
    result record;
    id uuid = uuid_generate_v4();


BEGIN
    --- Init master table value - Activity Log
    INSERT
        INTO ACTIVITY_LOG (ID, TABLE_NAME, RECORD_ID, ACTION)
    VALUES
        (id, TG_TABLE_NAME, NEW.ID, TG_OP);

    --- Loop through each column of the table that triggered and then insert into detail table - Activity Log Detail
    FOR result IN (
        SELECT
            COLUMN_NAME
        FROM
            INFORMATION_SCHEMA.COLUMNS
        WHERE
            TABLE_SCHEMA = TG_TABLE_SCHEMA
            AND TABLE_NAME = TG_TABLE_NAME
    )
        LOOP
            --- coalesce(row_to_json(OLD)::jsonb->result.column_name, '{}') <> coalesce(row_to_json(NEW)::jsonb->result.column_name, '{}')
            IF ((TG_OP = 'INSERT' OR TG_OP = 'UPDATE')) THEN
                INSERT INTO
                    ACTIVITY_LOG_DETAIL(ID, KEY, OLD_VALUE, NEW_VALUE, ACTIVITY_LOG_ID)
                VALUES
                    (uuid_generate_v4(), result.column_name, row_to_json(OLD)::jsonb->result.column_name, row_to_json(NEW)::jsonb->result.column_name, id);
            END IF;
        END LOOP;
    RETURN NEW;
END $$ LANGUAGE plpgsql;

--- Add trigger to specific table
CREATE or replace TRIGGER activity_log_trigger
    BEFORE INSERT OR UPDATE
    ON agency
    FOR EACH ROW
EXECUTE PROCEDURE activity_log();

CREATE or replace TRIGGER activity_log_trigger
    BEFORE INSERT OR UPDATE
    ON supplier
    FOR EACH ROW
EXECUTE PROCEDURE activity_log();


----------------------------------
insert into agency(id, name, email, address, credit)
values (uuid_generate_v4(), 'name', 'email', 'addr', 1);

insert into activity_log(id, table_name, record_id, action, created_at)
values (uuid_generate_v4(), 'name', uuid_generate_v4(), 'Test', now());

select *
from activity_log where action = 'Test';


delete
from activity_log
where 1=1;

delete
from activity_log_detail
where 1=1;


select *
from activity_log_detail;

select *
from activity_log;


SELECT activity_log_id, key, new_value
FROM activity_log_detail
ORDER BY 1,2;

select
    activity_log.id, table_name, action, key, old_value, new_value, activity_log_detail.id
from activity_log
inner join activity_log_detail on activity_log.id = activity_log_detail.activity_log_id
-- where table_name = 'supplier'
;


-- (
--
-- SELECT *
-- FROM   crosstab(
--    'SELECT activity_log_id, key, old_value
--     FROM activity_log_detail
--     ORDER BY 1,2'
-- ) AS ct (activity_log_id uuid, address varchar(100), credit varchar(100), email varchar(100), id varchar(100), name varchar(100))
--
--
-- inner join
-- (
-- select activity_log_id, address as old_address, credit as old_credit, email as old_email
--  from (
--     SELECT *
--     FROM   crosstab(
--        'SELECT activity_log_id, key, new_value
--         FROM activity_log_detail
--         ORDER BY 1,2'
--     ) AS ct (activity_log_id uuid, address varchar(100), credit varchar(100), email varchar(100), id varchar(100), name varchar(100))
-- )as old) as old
-- on ct.activity_log_id = old.activity_log_id) ;
--



SELECT
    *, ACTIVITY_LOG.ID, TABLE_NAME, ACTION, KEY, OLD_VALUE, NEW_VALUE, ACTIVITY_LOG_DETAIL.ID
FROM
    ACTIVITY_LOG
INNER
    JOIN ACTIVITY_LOG_DETAIL ON ACTIVITY_LOG.ID = ACTIVITY_LOG_DETAIL.ACTIVITY_LOG_ID
WHERE TABLE_NAME = 'supplier';





SELECT * FROM ACTIVITY_LOG_VIEW;
DROP VIEW ACTIVITY_LOG_VIEW;
CREATE OR REPLACE VIEW ACTIVITY_LOG_VIEW AS(
    SELECT
        ACTIVITY_LOG.ID AS ID, TABLE_NAME, ACTION, CREATED_AT,
        MAX
        (
            CASE
                WHEN key = 'id' AND action = 'INSERT' THEN new_value
                WHEN key = 'id' AND action <> 'INSERT' THEN old_value
            END
        ) AS table_ref_id,
        MAX(CASE WHEN key = 'name' THEN old_value END) AS old_name,
        MAX(CASE WHEN key = 'name' THEN new_value END) AS name,
        MAX(CASE WHEN key = 'email' THEN old_value END) AS old_email,
        MAX(CASE WHEN key = 'email' THEN new_value END) AS email,
        MAX(CASE WHEN key = 'address' THEN old_value END) AS old_address,
        MAX(CASE WHEN key = 'address' THEN new_value END) AS address,
        MAX(CASE WHEN key = 'credit' THEN old_value END) AS old_credit,
        MAX(CASE WHEN key = 'credit' THEN new_value END) AS credit,
        MAX(CASE WHEN key = 'phone' THEN old_value END) AS old_phone,
        MAX(CASE WHEN key = 'phone' THEN new_value END) AS phone,
        MAX(CASE WHEN key = 'account_no' THEN old_value END) AS old_account_no,
        MAX(CASE WHEN key = 'account_no' THEN new_value END) AS account_no,
        MAX(CASE WHEN key = 'status' THEN old_value END) AS old_status,
        MAX(CASE WHEN key = 'status' THEN new_value END) AS status
    FROM ACTIVITY_LOG
     INNER
         JOIN ACTIVITY_LOG_DETAIL ON ACTIVITY_LOG.ID = ACTIVITY_LOG_DETAIL.ACTIVITY_LOG_ID
    GROUP BY 1, 2, 3, 4
);


--- Show all trigger
SELECT  EVENT_OBJECT_TABLE AS TABLE_NAME ,TRIGGER_NAME
FROM INFORMATION_SCHEMA.TRIGGERS
GROUP BY TABLE_NAME , TRIGGER_NAME
ORDER BY TABLE_NAME ,TRIGGER_NAME;

--- Drop trigger
drop trigger activity_log_trigger on agency;

