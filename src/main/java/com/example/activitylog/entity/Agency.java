package com.example.activitylog.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

@Setter
@Getter
@Entity
public class Agency {
    @Id
    private UUID id;
    private String name;
    private String email;
    private String address;
    private BigDecimal credit;
}
