package com.example.activitylog.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Entity
public class Supplier {

    @Id
    private UUID id;
    private String phone;
    private String address;
    private String accountNo;
    private boolean status;
}
