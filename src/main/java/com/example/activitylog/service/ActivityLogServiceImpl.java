package com.example.activitylog.service;


import com.example.activitylog.entity.ActivityLog;
import com.example.activitylog.repository.ActivityLogRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ActivityLogServiceImpl {

    private final ActivityLogRepository activityLogRepository;


    public List<ActivityLog> findAll() {
        return activityLogRepository.findAll();
    }
}
