package com.example.activitylog.service;


import com.example.activitylog.dto.AddSupplierRequestDto;
import com.example.activitylog.dto.UpdateSupplierInfoRequestDto;
import com.example.activitylog.entity.Supplier;
import com.example.activitylog.repository.SupplierRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class SupplierServiceImpl {

    private final SupplierRepository supplierRepository;


    public void updateCredit(UpdateSupplierInfoRequestDto updateSupplierInfoRequestDto) {
        Supplier supplier = supplierRepository.findById(updateSupplierInfoRequestDto.getSupplierId()).orElseThrow();
        BeanUtils.copyProperties(updateSupplierInfoRequestDto, supplier);

        /// Save
        supplierRepository.save(supplier);
    }

    public Supplier addNew(AddSupplierRequestDto addSupplierRequestDto) {
        Supplier supplier = new Supplier();
        BeanUtils.copyProperties(addSupplierRequestDto, supplier);
        supplier.setId(UUID.randomUUID());
        supplier.setStatus(true);

        return supplierRepository.save(supplier);
    }

    public List<Supplier> findAll() {
        return supplierRepository.findAll();
    }
}
