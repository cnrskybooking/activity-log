package com.example.activitylog.service;


import com.example.activitylog.dto.AddAgencyRequestDto;
import com.example.activitylog.dto.UpdateAgencyCreditRequestDto;
import com.example.activitylog.entity.Agency;
import com.example.activitylog.repository.AgencyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class AgencyServiceImpl {

    private final AgencyRepository agencyRepository;


    public void updateCredit(UpdateAgencyCreditRequestDto updateAgencyCreditRequestDto) {
        Agency agency = agencyRepository.findById(updateAgencyCreditRequestDto.getAgencyId()).orElseThrow();

        /// Update amount
        BigDecimal amount = switch (updateAgencyCreditRequestDto.getModifyType()) {
            case DECREMENT -> updateAgencyCreditRequestDto.getAmount().multiply(new BigDecimal(-1));
            case INCREMENT -> updateAgencyCreditRequestDto.getAmount();
        };
        agency.setCredit(agency.getCredit().add(amount));

        /// Save
        agencyRepository.save(agency);
    }

    public Agency addNew(AddAgencyRequestDto addAgencyRequestDto) {
        Agency agency = new Agency();
        BeanUtils.copyProperties(addAgencyRequestDto, agency);
        agency.setId(UUID.randomUUID());

        return agencyRepository.save(agency);
    }

    public List<Agency> findAll() {
        return agencyRepository.findAll();
    }
}
