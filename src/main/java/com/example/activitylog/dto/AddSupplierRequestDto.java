package com.example.activitylog.dto;

import lombok.Data;


@Data
public class AddSupplierRequestDto {
    private String phone;
    private String address;
    private String accountNo;
}
