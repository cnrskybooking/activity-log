package com.example.activitylog.dto;

import com.example.activitylog.vo.CreditModifyType;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;


@Data
public class UpdateAgencyCreditRequestDto {
    private UUID agencyId;
    private BigDecimal amount;
    private CreditModifyType modifyType;

}
