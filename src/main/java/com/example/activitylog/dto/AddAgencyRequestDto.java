package com.example.activitylog.dto;

import lombok.Data;

import java.math.BigDecimal;


@Data
public class AddAgencyRequestDto {
    private String name;
    private String email;
    private String address;
    private BigDecimal credit;
}
