package com.example.activitylog.controller;

import com.example.activitylog.entity.ActivityLog;
import com.example.activitylog.service.ActivityLogServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class ActivityLogController {
    private final ActivityLogServiceImpl activityLogService;


    @GetMapping("/logs")
    public List<ActivityLog> findAll() {
        return activityLogService.findAll();
    }
}
