package com.example.activitylog.controller;

import com.example.activitylog.dto.AddSupplierRequestDto;
import com.example.activitylog.dto.UpdateSupplierInfoRequestDto;
import com.example.activitylog.entity.Supplier;
import com.example.activitylog.service.SupplierServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RequiredArgsConstructor
@RestController
public class SupplierController {
    private final SupplierServiceImpl supplierService;

    @PutMapping("/suppliers")
    public void updateCredit(UpdateSupplierInfoRequestDto updateSupplierInfoRequestDto) {
        supplierService.updateCredit(updateSupplierInfoRequestDto);
    }

    @PostMapping("/suppliers")
    public Supplier addNew(AddSupplierRequestDto addSupplierRequestDto) {
        return supplierService.addNew(addSupplierRequestDto);
    }

    @GetMapping("/suppliers")
    public List<Supplier> findAll() {
        return supplierService.findAll();
    }
}
