package com.example.activitylog.controller;

import com.example.activitylog.dto.AddAgencyRequestDto;
import com.example.activitylog.dto.UpdateAgencyCreditRequestDto;
import com.example.activitylog.entity.Agency;
import com.example.activitylog.service.AgencyServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RequiredArgsConstructor
@RestController
public class AgencyController {
    private final AgencyServiceImpl agencyService;

    @PutMapping("/agencies")
    public void updateCredit(UpdateAgencyCreditRequestDto updateAgencyCreditRequestDto) {
        agencyService.updateCredit(updateAgencyCreditRequestDto);
    }

    @PostMapping("/agencies")
    public Agency addNew(AddAgencyRequestDto addAgencyRequestDto) {
        return agencyService.addNew(addAgencyRequestDto);
    }

    @GetMapping("/agencies")
    public List<Agency> findAll() {
        return agencyService.findAll();
    }
}
