package com.example.activitylog.vo;

public enum CreditModifyType {
    INCREMENT("INCREMENT"), DECREMENT("DECREMENT");
    private final String type;

    CreditModifyType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
