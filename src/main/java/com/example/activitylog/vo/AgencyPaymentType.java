package com.example.activitylog.vo;

public enum AgencyPaymentType {
    PRE_PAYMENT("PRE_PAY"), INVOICE("INVOICE");
    private final String type;

    AgencyPaymentType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
